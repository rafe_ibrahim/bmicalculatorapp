//
//  TableViewController.swift
//  bmiCalculatorApp
//
//  Created by Rafe Ibrahim on 2.4.2020.
//  Copyright © 2020 Rafe Ibrahim. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var persons: Array<Person> = Array()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // loadSamplePersons()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    //loading sample data
    
//    private func loadSamplePersons() {
//
//    let person1 = Person(name: "mark1", age: 50, height: 170, weight: 90)
//
//    let person2 = Person(name: "mark2", age: 50, height: 170, weight: 90)
//
//    let person3 = Person(name: "mark3", age: 50, height: 170, weight: 90)
//
//    persons += [person1, person2, person3]
//    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        print("persons.count", String(persons.count))
        return persons.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        // Table view cells are reused and should be dequeued using a cell identifier
        let cellIdentifier = "PersonTableViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PersonTableViewCell else {
            fatalError("The dequeued cell is not an instance of PersonTableViewCell.")
        }

        // Fetches the appropriate meal for the data source layout
        let person = persons[indexPath.row]

        // Configure the cell...

        cell.nameLabel.text = person.name
        cell.heightLabel.text = String(person.height)
        cell.weightLabel.text = String(person.weight)
        cell.bmiLabel.text = String(format: "%.1f", person.BMI)
        return cell
    }
    
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
