//
//  ViewController.swift
//  bmiCalculatorApp
//
//  Created by Rafe Ibrahim on 1.4.2020.
//  Copyright © 2020 Rafe Ibrahim. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var tempPersons: Array<Person> = Array()
    var heightData: Array<Int> = Array()
    var weightData: Array<Int> = Array()
    let maxWeight = 150
    let minWeight = 30
    let maxHeight = 250
    let minHeight = 100
    var selectedWeight = 30
    var selectedHeight = 100
    
    @IBOutlet weak var inputName: UITextField!
    @IBOutlet weak var whPicker: UIPickerView!
    @IBOutlet weak var calculateBMIBtn: UIButton!
    @IBOutlet weak var bmiLabel: UILabel!
    @IBOutlet weak var saveBMIBtn: UIButton!
    
    @IBOutlet weak var historyButton: UIBarButtonItem!
    var person: Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        bmiLabel.accessibilityIdentifier = "bmiLabel"
        
        // Connect Data
        inputName.delegate = self as UITextFieldDelegate
        whPicker.delegate = self as UIPickerViewDelegate
        whPicker.dataSource = self as UIPickerViewDataSource
        
        
        // Input the data into the array
        
        for height in minHeight...maxHeight {
            heightData.append(height)
        }
        
        for weight in minWeight...maxWeight {
            weightData.append(weight)
        }
        
        saveBMIBtn.isEnabled = false
        
        self.tempPersons.removeAll()
    }
    
//    It calls the textFieldShouldReturn(_:) method when the user taps the keyboard’s return button.
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        inputName.resignFirstResponder()
        return true
    }
    
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // if you need somthing to be done after user has done editing name text field
        if (inputName.text != nil) {
            // is it write to force unwrap here??
            if (inputName.text!.count >= 3) {
                print("condition working")
                saveBMIBtn.isEnabled = true
            }
        }
    }
    
    // Number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    // Number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (component == 1) {
            return heightData.count
        } else {
            return weightData.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (component == 1) {
            return String(heightData[row])
        } else {
            return String(weightData[row])
        }
    }
    
    // Called by the picker view when the user selects a row in a component.
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow: Int, inComponent: Int) {
        if(inComponent == 1) {
            self.selectedHeight = heightData[didSelectRow]
        }
        if(inComponent == 0) {
        self.selectedWeight = weightData[didSelectRow]
        }
        
    }
    
    @IBAction func calculate(_ sender: UIButton) {
        // calculate BMI and display it in BMILable
        let heightInMeters = Double(selectedHeight)/100.0
        print ("selectedHeight", selectedHeight)
        print ("selectedWeight", selectedWeight)
        let bmi = Double(selectedWeight)/(heightInMeters * heightInMeters)
        print("bmi", bmi)
        bmiLabel.text = String(format: "%.1f", bmi)
    }
    
    
    @IBAction func saveBMI(_ sender: UIButton) {
        print ("selectedHeightSave", selectedHeight)
        print ("selectedWeightSave", selectedWeight)
        
        guard let name = inputName.text else {
            fatalError("person cannot be created with empty name")
        }
        
        let person = Person(name: name, height: self.selectedHeight, weight: self.selectedWeight)
        
        if let createdPerson = person {
            createdPerson.printPerson()
            self.tempPersons.append(createdPerson)
        } else {
            print("person instance could not be created!")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if (segue.identifier == "historyScreen") {
            if let destination = segue.destination as? TableViewController {
                destination.persons = self.tempPersons
            }
        }
        
    }
    

}

