//
//  Person.swift
//  bmiCalculatorApp
//
//  Created by Rafe Ibrahim on 2.4.2020.
//  Copyright © 2020 Rafe Ibrahim. All rights reserved.
//

import UIKit

class Person {
    private(set) var name: String
    private(set) var height: Int
    private(set) var weight: Int
    
    var BMI:Double {
        get {
            let heightInMeters = Double(height)/100.0
            return Double(weight)/(heightInMeters * heightInMeters)
        }
    }
    
    init? (name:String, height:Int, weight:Int) {
        
        // The name must not be empty or less than 3 characters
        guard !name.isEmpty && name.count > 2 else {
            return nil
        }
        
        // The height must be between 100 - 250 cm
        guard height >= 100 && height <= 250 else {
            return nil
        }
        
        // The weight must be between 30 - 150 kg
        guard weight >= 30 && weight <= 150 else {
            return nil
        }
        
        self.name = name
        self.height = height
        self.weight = weight
    }
    
    func printPerson () {
        print("New person instance created")
        print("Name: \(self.name)")
        print("Height: \(self.height)")
        print("Weight: \(self.weight)")
    }
}
