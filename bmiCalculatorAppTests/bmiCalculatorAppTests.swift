//
//  bmiCalculatorAppTests.swift
//  bmiCalculatorAppTests
//
//  Created by Rafe Ibrahim on 1.4.2020.
//  Copyright © 2020 Rafe Ibrahim. All rights reserved.
//

import XCTest
@testable import bmiCalculatorApp

class bmiCalculatorAppTests: XCTestCase {
    let myPerson = Person(name: "Jack", height: 165, weight: 80)
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testConstructionWithLegalValues() {
        let person2 = Person(name: "Andrew", height: 176, weight: 100)
        XCTAssert(person2?.name == "Andrew", "Incorrect name value")
        XCTAssert(person2?.height == 176, "Incorrect height value")
        XCTAssert(person2?.weight == 100, "Incorrect weight value")
    }
    
    func testConstructionWithIllegalValues() {
        // illegal name value
        let person3 = Person(name: "", height: 176, weight: 100)
        XCTAssert(person3 == nil, "Succeeded in creating person instance with empty name")
        
        // illegal height value
        let person4 = Person(name: "Jack", height: 50, weight: 100)
        XCTAssert(person4 == nil, "Succeeded in creating person instance with height less than 100")
        
        // illegal weight value
        let person5 = Person(name: "Henry", height: 176, weight: 10)
        XCTAssert(person5 == nil, "Succeeded in creating person instance with weight less than 30")
        
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
